package utp_zaoczne_21;

import java.util.function.Function;

class InputConverter <T>{
    T data;

    public InputConverter(T data) {
        this.data = data;
    }

    public <R> R convertBy(Function... functions) {
        Object result = functions[0].apply(data);
        for(int i = 1; i < functions.length; i++) {
            result = functions[i].apply(result);
        }
        return (R) result;
    }

}
