package utp_zaoczne_22;

import java.util.NoSuchElementException;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class Maybe<T> {
    private T value;

    private Maybe(T value) {
        this.value = value;
    }

    public static <T> Maybe<T> of(T t) {
        return new Maybe<>(t);
    }

    @Override
    public String toString() {
        return value != null ? "Maybe has value " + value : "Maybe is empty";
    }

    public boolean isPresent() {
        return value != null;
    }

    public void ifPresent(Consumer<? super T> consumer) {
        if (isPresent())
            consumer.accept(value);
    }

    public <R> Maybe<R> map(Function<? super T, ? extends R> function) {
        if (!isPresent())
            return empty();
        return of(function.apply(value));
    }

    public T get() {
        if (!isPresent())
            throw new NoSuchElementException("maybe is empty");
        return value;
    }

    public T orElse(T replacement) {
        if (!isPresent())
            return replacement;
        return get();
    }

    private static <T> Maybe<T> empty() {
        return of(null);
    }

    public Maybe<T> filter(Predicate<T> predicate) {
        return predicate.test(value) ? this : empty();
    }

}
