/**
 *
 *  @author Bernat-Januszkiewicz Kacper S16508
 *
 */

package utp_zaoczne_23;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class Anagrams {
    private Map<String, List<String>> angrms;

    private String createMark(String s) {
        StringBuilder stringBuilder = new StringBuilder();
        s.toLowerCase().chars().sorted().forEach(i -> stringBuilder.append((char)i));
        return stringBuilder.toString();
    }

    public Anagrams (String data) {
        angrms = new HashMap<>();
            try {
                List<String> in = Files.readAllLines(Paths.get(data));
                List<String> strings = new LinkedList<>();
                for(String s : in) {
                    strings.addAll(Arrays.asList(s
                        .split(" ")));
                }

                for(String s : strings) {
                    String mark = createMark(s);
                    if(!angrms.containsKey(mark)) {
                        angrms.put(mark, new LinkedList<>());
                    }
                    angrms.get(mark).add(s);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

    }

    public List<List<String>> getSortedByAnQty() {
        return angrms.values().stream().sorted(Comparator.comparing(
                l -> createMark(l.get(0)))).collect(Collectors.toList());
    }

    public String getAnagramsFor(String s) {
        List<String> anagrams = angrms.get(createMark(s));
        anagrams.remove(s);
        return s + ": " + anagrams;
    }
}


