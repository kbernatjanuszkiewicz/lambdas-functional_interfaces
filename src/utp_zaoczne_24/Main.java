/**
 *
 *  @author Bernat-Januszkiewicz Kacper S16508
 *
 */

package utp_zaoczne_24;
import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.*;

public class Main {
    private static String canonicalize(String s) {
        return Stream.of(s.split("")).sorted().collect(Collectors.joining(""));
    }

    public static Map<String, Set<String>> getAnagrams(Reader reader) {
        Map<String, List<String>> map = new BufferedReader(reader).lines()
                .flatMap(Pattern.compile("\\W+")::splitAsStream)
                .distinct() // remove repeating words
                .collect(Collectors.groupingBy(Main::canonicalize));
        return map.values().stream()
                .filter(list -> list.size() > 1)
                .collect(Collectors.toMap(list -> list.get(0),
                        list -> new TreeSet<>(list.subList(1, list.size()))));
    }

    public static void main(String[] args) throws IOException {
        getAnagrams(new InputStreamReader(new URL("http://wiki.puzzlers.org/pub/wordlists/unixdict.txt").openStream(), StandardCharsets.UTF_8))
                .entrySet().forEach(System.out::println);
    }

//S



}
